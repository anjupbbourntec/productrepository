package Main;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * A interface specifies the operations on Student object
 * 
 * @author Anju
 *
 *
 */
public class ProductServiceArrayImpl implements ProductService {

	Product[] productArray;
	int count=0;
	
	public ProductServiceArrayImpl()
	{
		productArray=new Product[100];
	}
	public ProductServiceArrayImpl(int size)
	{
		productArray=new Product[size];
	}
	
 public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	/**
	 * The method will add a product
	 * 
	 * @param product product to be added
	 *
	 */
public void add(Product product) {
	
	 boolean flag = true;
	 if (count >= productArray.length)
	     System.out.println("Data Limit exceeded");
	 else if (flag == true) 
	 {
	  for (int i = 0; i < count; i++) 
	  {
	    if (productArray[i].equals(product)) 
	    {
	     System.out.println("Duplicate Data");
	     flag = false;
	    }
	  }
	 }
	 if (flag == true) 
	 {
		 productArray[count] = product;
		 count++;
	 }

}


/**
* The method will display all the products
*/
public void displayAll()
{
	if(count==0)
		System.out.println("No Data");
	else 
	{
		for(int i=0;i<count;i++)
		{
			System.out.println(productArray[i]);
		}
	}
}

/**
* The method will sort the products and display all products
*/
public void sort() {
	 Arrays.sort(productArray,0,count);
	 displayAll();
}

/**
* The method will search products and display the product with given date
 * @param  
* @param date of the product to be searched
*/
public List<Product> searchProduct(LocalDate createdDate) {
		
	List<Product>productList = new ArrayList<Product>();
	for (int i = 0; i < count; i++)
	   {
		   if(productArray[i].getCreatedDate().isBefore(createdDate))
		   {
			   productList.add(productArray[i]);
		   }
	   }
	return productList;
	
}


/**
* The method will download the details of product as JSON String
*/

@Override
public void downloadJSON() throws IOException {
	
	String data;
	FileWriter file = new FileWriter("downloadProduct.json");
	for (int i = 0; i < count; i++) {
	if (i == 0)
	data = "[" + getJSON(productArray[i]);
	else
	data = "," + getJSON(productArray[i]);
	if (i == count - 1)
	data = data + "]";
	// data = join.add(getJSON(customerArray[i])).toString();
	file.write(data);
	}
	file.close();
}

/**
* The method will delete the products based on the type.
*/
@Override
public void delete(String type) {
	
	int k=0;
	for (int i = 0; i <count; i++) {
		if (!productArray[i].getType().equals(type)){
			productArray[k] = productArray[i];
		    k++;
		    System.out.println("Successfully deleted");
		}
	}
	count=k;
	
}

/**
* The method will download the details of a product in a CSV file
*/
@Override
public void download() throws IOException {
	
	FileWriter file = new FileWriter("ProductDetails.csv");
	for (int i = 0; i < count; i++) {
	String result = getCSVString(productArray[i]);
	System.out.println(result);
	file.write(result);
	}
	file.close();
	
}

/**
* The method will search products and display the product with given id
* @param id of the product to be searched
*
*/
@Override
public void searchProduct(String type) {
	// TODO Auto-generated method stub
	int flag=0;
	if(productArray[0]==null)
		System.out.println("No Data");
	else {
	for (int i = 0; i < count; i++)
	   {
		   if(productArray[i].getType()==type)
		   {
			   System.out.println(productArray[i]);
			   flag=1;
		   }
	   }
	
}
}
	
}



