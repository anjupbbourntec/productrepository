package Main;
import java.awt.List;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Properties;
import java.util.Scanner;

public class MenuDriven {

	public static void main(String[] args) throws Exception {
		
		Scanner scanner = new Scanner(System.in);
		
		FileReader reader= new FileReader("src/config.properties");
        Properties properties= new Properties();
        properties.load(reader);
               
		//ProductService productService = new ProductServiceArrayImpl();
		//ProductService productService = new ProductServiceArrayListImpl();
		//ProductService productService = ProductServiceFactory.getProductService("ArrayList");
        ProductService productService = ProductServiceFactory.getProductService(properties);
		int choice;
		
				
		do
		{
			System.out.println("Select Your Choice:");
			System.out.println();
			
			System.out.println("1.Enter the details of product");
			System.out.println("2.Display the details of product");
			System.out.println("3.Search details");
			System.out.println("4.Sort");
			System.out.println("5.Search with Created Date");
			System.out.println("6.Download Product Details in CSV file");
			System.out.println("7.Download Product Details in Json file");
			System.out.println("8.Delete Product");
			System.out.println("9.Exit");
			System.out.println();
			System.out.println("Enter the choice:");
		    choice = scanner.nextInt();
		        
		    switch(choice){
	        case 1:
	        	
	        	System.out.println();		        	
	        	System.out.println("Enter the details of product:");
	        	System.out.println();
	        	
	        	System.out.println("Enter the Type:");
	        	String type=scanner.next();
	        	System.out.println("Enter the Id:");
	        	int id=scanner.nextInt();
	        	System.out.println("Enter the Cost:");
	        	double cost=scanner.nextDouble();
	        	System.out.println("Enter the Date:");
	        	String date=scanner.next();
	        	LocalDate createdDate = LocalDate.parse(date); 
	        	System.out.println();
	        	System.out.println("Enter the Size:");
	        	String s=scanner.next();
	        	ProductSize size = ProductSize.valueOf(s);
	        	Product product = new Product(id,cost,type,createdDate,size);
	        	productService.add(product);
				break;
	        case 2:
	        	productService.displayAll();	
				break;
	        case 3:
	        	System.out.println("Enter the Product type for details:");
	            String poducttype = scanner.next();	
	        	productService.searchProduct(poducttype);
	        	break;
	        case 4:
	        	productService.sort();
	        	break;
	        case 5:
	        	System.out.println("Enter the Created Date for details:");
	        	LocalDate searchDate = LocalDate.parse(scanner.next()); 
	        	productService.searchProduct(searchDate);
	        	List products =(List) productService.searchProduct(searchDate);
		        System.out.println(products);
		        if(products==null)
		        System.out.print("No Data Found");
	        	break;
	        case 6:
	        	try 
	        	{
	        		productService.download();
	        	} 
	        	catch (IOException e)
	        	{
	        		 e.printStackTrace();
	        	}

	        	break;
	        	case 7:
	        		try 
		        	{
	        			productService.downloadJSON();
		        	} 
		        	catch (IOException e)
		        	{
		        		 e.printStackTrace();
		        	}

		        	break;
	        	case 8:
	        		System.out.println("Enter the Product type for delete:");
		            String poductType = scanner.next();	
		        	productService.delete(poductType);;
		        	break;
	        	case 9:
	        	System.exit(0);
	        	break;
	        	default:
	        	System.out.println("Invalid entry");
	        	break;
		    } 
			
		    
		}while(choice!=9);	
		}

	}


