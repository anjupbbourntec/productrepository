package Main;


import java.util.Scanner;
import java.time.LocalDate;;

public class Product implements Comparable<Product>
{
	private int id ;
	private String type;
	private double cost;
	private LocalDate createdDate;
	private ProductSize size;
		
	 public Product( int id, double cost, String type,LocalDate createdDate,ProductSize size) {
		
		 this.id= id;
		 this.type= type;
		 this.cost=cost;
		 this.createdDate = createdDate;
		 this.size = size;
	}

	public Product() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	public void readProduct(Scanner scanner) {
		System.out.println("Enter the Type:");
		type=scanner.next();
		System.out.println("Enter the Id:");
		id=scanner.nextInt();
		System.out.println("Enter the Cost:");
		cost=scanner.nextDouble();
		System.out.println();
		System.out.println("Enter the Created Date:");
		String date=scanner.next();
		this.createdDate = LocalDate.parse(date); 
		
	}	
      
	public String toString() {
		
		return new StringBuffer().append("Id:").append(id).append("\n").append("Type:")
				.append(type).append("\n").append("Cost:").append(cost).append("\n")
				.append("Size:").append(size).append("\n")
				.append("Created Date:").append(createdDate).toString();
	}
	
public void displayProduct() {
	
	System.out.println("Product Id : "+id);
	System.out.println("Product Type : "+type);
	System.out.println("Product Cost : "+cost);
	System.out.println("Created Date : "+createdDate);
	System.out.println();
	
}

public ProductSize getSize() {
	return size;
}

public void setSize(ProductSize size) {
	this.size = size;
}

@Override
public int compareTo(Product obj) {
	
	return this.id- obj.id;
				 
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Product other = (Product) obj;
	if (Double.doubleToLongBits(cost) != Double.doubleToLongBits(other.cost))
		return false;
	if (createdDate == null) {
		if (other.createdDate != null)
			return false;
	} else if (!createdDate.equals(other.createdDate))
		return false;
	if (id != other.id)
		return false;
	if (size != other.size)
		return false;
	if (type == null) {
		if (other.type != null)
			return false;
	} else if (!type.equals(other.type))
		return false;
	return true;
}



     
}
