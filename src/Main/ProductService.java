package Main;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
* A interface specifies the operations on Product object
*  @author anju
*/
public interface ProductService {
	
	/**
	* The method will add a Product
	* @param product to be added
	*/
	void add(Product product);
	
	/**
	* The method will display all the products
	*/
	void displayAll();
	
	/**
	* The method will sort the products and display all products
	*/
	void sort();
	
	/**
	* The method will search products and display the product with given date
	 * @param  
	* @param date of the product to be searched
	*/
	List<Product>searchProduct(LocalDate createddate);
	
	/**
	* The method will search products and display the product with given type
	* @param type of the product to be searched
	*
	*/
	void searchProduct(String type);
	
	/**
	* The method will delete the products based on the type.
	*/
	void delete(String type);
	
	/**
	* The method will return the details of a product as a csv string
	*/
	 default String getCSVString(Product product)
	 {
		 
	 return new StringBuffer()
			 .append(product.getId()).append(",")
			 .append(product.getType()).append(",")
			 .append(product.getCost()).append(", ")
			 .append(product.getSize()).append(", ")
			 .append(product.getCreatedDate())
			 .toString();
	
	 }
	
	 
	/**
	* The method will return the details of a product as a json string
	*/
	default String getJSON(Product product) 
	{
		return new StringBuffer().append("{").append("\"Id\":").append("\"").append(product.getId())
		.append("\"").append(",").append("\"Type\":").append(product.getType()).append(",").append("\"Size\":")
		.append("\"").append(product.getSize()).append("\"").append(",").append("\"Cost\":").append("\"")
		.append(product.getCost()).append("\"").append(",").append("\"Created Date\":")
		.append(product.getCreatedDate()).append("}").append("\n").toString();
	}

	/*
	* The method will download the product in details in a CSV file
   */

	void download() throws IOException;

	/*
	* The method will download the product in details in a json file
    */

	void downloadJSON() throws IOException;
}
