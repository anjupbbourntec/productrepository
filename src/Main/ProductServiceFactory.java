package Main;

import java.util.Properties;

public abstract class ProductServiceFactory 
{
	public static ProductService getProductService(Properties properties) 
	{
		String type=properties.getProperty("implementation");
		ProductService service= null;
		switch(type)
		{
		case "ARRAY" :
			service = new ProductServiceArrayImpl();
			break;
		case "ARRAYLIST" :
			service = new ProductServiceArrayListImpl();
			break;
		case "DATABASE" :
	        String driver = properties.getProperty("driver");
	        String url = properties.getProperty("url");
	        String user = properties.getProperty("user");
	        String password = properties.getProperty("password");
	        service = new ProductServiceDatabaseImplementation(driver,url,user,password);
			break;
		}
		return service;
		
	}


}
