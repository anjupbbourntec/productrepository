package Main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class ProductServiceArrayListImpl implements ProductService {

	List<Product> productList = new ArrayList();
	int count=0;
	@Override
	public void add(Product product)
	{
		if (productList.size() > 0 && productList.contains(product)) {
			System.out.println("Already exist");
		} else {

			productList.add(product);
		}
		count++;
		
	}

	@Override
	public void displayAll() {
		
		//ProductList.forEach(list -> System.out.println("*\n" + list));
		productList.forEach(data->System.out.println(":::\n"+data+":::"));
		//productList.forEach(System.out::println);
		
	}	
	
	@Override
	public void sort() {
		// TODO Auto-generated method stub
		Collections.sort(productList);
		displayAll();
		
	}
	
//	public static Comparator<Product> sortByType = new Comparator<Product>() {
//		@Override
//		public int compare(Product obj1, Product obj2) {
//		return obj1.getType()-obj2.getType();
//		}

	@Override
	public List<Product> searchProduct(LocalDate createddate) {
		
		return productList.stream()
		.filter(ProductList->ProductList.getCreatedDate().isAfter(createddate))
		.collect(Collectors.toList());
		
		
		
	}

	@Override
	public void searchProduct(String type) {
		// TODO Auto-generated method stub
		for (Product search : productList) {
			if (search.getType().equalsIgnoreCase(type)) {
			System.out.println(type);
			}
			}
		
	}

	@Override
	public void delete(String type) 
	{
		
		Iterator<Product> iterator = productList.iterator();
		Product product=iterator.next();
				if (product.getType().equals(type)) 
				{
					iterator.remove();
					System.out.println("Successfully deleted");
				}

	    }	
//		Iterator<Product> iterator = productList.iterator();
//		while (iterator.hasNext()) {
//			Product products = (Product)iterator.next();
//		for(int i=0; i<productList.size();i++) {
//		if (products.getType().equals(type))
//		{
//		iterator.remove();
//		System.out.println("Successfully deleted");
//		}
//		}
//		

		
	

	@Override
	public void download() throws IOException {
		// TODO Auto-generated method stub
		FileWriter file = new FileWriter("download.csv");
		for (int i = 0; i < count; i++) {
			String data = getCSVString(productList.get(i));
			file.write(data);
		}
		file.close();
		
	}

	@Override
	public void downloadJSON() throws IOException {
		// TODO Auto-generated method stub
		String data;
		StringJoiner join = new StringJoiner(",", "[", "]");
		FileWriter file = new FileWriter("download.json");
		for (Product product : productList) {
		data = getJSON(product);
		join.add(data);
		}file.write(join.toString());
		file.close();
		
		
	}

	

}
