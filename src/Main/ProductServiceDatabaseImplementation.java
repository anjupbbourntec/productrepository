package Main;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class ProductServiceDatabaseImplementation implements ProductService {

	String user,password,driver,url;
	
	public ProductServiceDatabaseImplementation(String driver, String url,String user, String password) {
		
		this.driver = driver;
		this.url = url;
		this.user = user;
		this.password = password;
	}

	public ProductServiceDatabaseImplementation() {
		// TODO Auto-generated constructor stub
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException
	{
	Class.forName(driver);
	return DriverManager.getConnection(url,user,password);
	}
	
	private Product getProduct(ResultSet resultSet) throws SQLException
	{
	Product product=new Product();
	product.setId(resultSet.getInt("id"));
	product.setType(resultSet.getString("type"));
	product.setCost(resultSet.getInt("cost"));
	product.setSize(ProductSize.valueOf(resultSet.getString("size")));
	product.setCreatedDate(resultSet.getDate("created_date").toLocalDate());
	return product;
	}


	@Override
	public void add(Product product) {
		 try
		 {
		 Connection connection=getConnection();

		 String query="INSERT INTO product(id,type,cost,created_date,size) Values(?,?,?,?,?)";
		 PreparedStatement preparedStatement=connection.prepareStatement(query);
		    preparedStatement.setInt(1,product.getId());
	        preparedStatement.setString(2,product.getType());
	        preparedStatement.setDouble(3,product.getCost());
	        preparedStatement.setDate(4,Date.valueOf(product.getCreatedDate()));
	        preparedStatement.setString(5,product.getSize().toString());

	     preparedStatement.executeUpdate();
	     connection.close();

		 }
		 catch(Exception e)
		 {
		 e.printStackTrace();
		 System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		 System.exit(0);
		 }
		 System.out.println();
		 System.out.println("VALUES ARE SUCESSFULLY INSERTED");
		 }


	@Override
	public void displayAll() 
	{
		// TODO Auto-generated method stub       
		try {
			Connection connection = getConnection();
			String query = "Select * from product";
	        PreparedStatement preparedStatement= connection.prepareStatement(query);
	        ResultSet resultSet = preparedStatement.executeQuery();
	         while(resultSet.next())
	         { 
	        	 Product product=getProduct(resultSet);
	        	 System.out.println(product) ;
	        	 System.out.println();
	         }
	       
	        resultSet.close();
	        preparedStatement.close();
	        connection.close();
		}catch(Exception e)
		{
		e.printStackTrace();
		System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		System.exit(0);
		}
		System.out.println();
		System.out.println("VALUES ARE SUCESSFULLY DISPLAYED");
	}

		
	

	@Override
	public void sort() {
		// TODO Auto-generated method stub
		try {
			Connection connection = getConnection();
			String query = "SELECT * FROM PRODUCT ORDER BY type" ;
	        PreparedStatement preparedStatement= connection.prepareStatement(query);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        while(resultSet.next())
	         { 
	        	 Product product=getProduct(resultSet);
	        	 System.out.println(product) ;
	        	 System.out.println();
	         }
	        resultSet.close();
	        preparedStatement.close();
	        connection.close();
		}catch(Exception e)
		{
		e.printStackTrace();
		System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		System.exit(0);
		}
		System.out.println();
		System.out.println("VALUES ARE SUCESSFULLY SORTED");
		
	}

	@Override
	public List<Product> searchProduct(LocalDate createddate) {
		// TODO Auto-generated method stub
		List<Product>productlist = new ArrayList<Product>();
		try {
			Connection connection = getConnection();
			
	        PreparedStatement preparedStatement= connection.prepareStatement("SELECT * FROM PRODUCT WHERE created_date=?");
	        preparedStatement.setDate(1, Date.valueOf(createddate));
	        ResultSet resultSet = preparedStatement.executeQuery();
	        while(resultSet.next())
	         { 
	        	 Product product=getProduct(resultSet);
	        	 System.out.println(product) ;
	        	 System.out.println();
	         }
	        resultSet.close();
	        preparedStatement.close();
	        connection.close();
		}catch(Exception e)
		{
		e.printStackTrace();
		System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		System.exit(0);
		}
		System.out.println();
		return productlist;
	}

	@Override
	public void searchProduct(String type) {
		// TODO Auto-generated method stub
		try {
			Connection connection = getConnection();
	        PreparedStatement preparedStatement= connection.prepareStatement("SELECT * FROM PRODUCT WHERE TYPE=?");
	        preparedStatement.setString(1, type);
	        ResultSet resultSet = preparedStatement.executeQuery();
	        
	        while(resultSet.next())
	         { 
	        	 Product product=getProduct(resultSet);
	        	 System.out.println(product) ;
	        	 System.out.println();
	         }
	        resultSet.close();
	        preparedStatement.close();
	        connection.close();
		}catch(Exception e)
		{
		e.printStackTrace();
		System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		System.exit(0);
		}
		System.out.println();
		
	}

	@Override
	public void delete(String type) {
		// TODO Auto-generated method stub      
				try {
					Connection connection = getConnection();
					String query = "DELETE FROM PRODUCT WHERE type="+type;
			        PreparedStatement preparedStatement= connection.prepareStatement(query);
			        ResultSet resultSet = preparedStatement.executeQuery();
			        resultSet.close();
			        preparedStatement.close();
			        connection.close();
				}catch(Exception e)
				{
				e.printStackTrace();
				System.err.println( e.getClass().getName()+": "+ e.getMessage() );
				System.exit(0);
				}
				System.out.println();
				System.out.println("VALUES ARE SUCESSFULLY DELETED");
		
	}

	@Override
	public void download() throws IOException {
		// TODO Auto-generated method stub
		Connection connection;
		try {
		connection = getConnection();
		PreparedStatement prestatement = connection.prepareStatement("SELECT * FROM PRODUCT");
		ResultSet resultSet = prestatement.executeQuery();
		FileWriter file = new FileWriter("Productdatabasedetails.csv");
		while (resultSet.next()) {

		String data =getCSVString(getProduct(resultSet));
		file.write(data);
		}
		file.close();
		} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
	}

	@Override
	public void downloadJSON() throws IOException {
		// TODO Auto-generated method stub
		Connection connection;
		try {
		connection = getConnection();
		PreparedStatement prestatement = connection.prepareStatement("SELECT * FROM PRODUCT");
		ResultSet resultSet = prestatement.executeQuery();
		FileWriter file = new FileWriter("ProductDetails.json");
		StringJoiner join = new StringJoiner(",", "[", "]");
		while (resultSet.next()) {
		String data = join.add(getJSON(getProduct(resultSet))).toString();
		file.write(data);

		}
		file.close();
		connection.close();
		} catch (ClassNotFoundException | SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
	}

}
